package utemezo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class Main {

	public static char elozo;

	public static void main(String[] args) {

		ArrayList<Taszk> taszkok = new ArrayList<Taszk>();

		Scanner olvaso = new Scanner(System.in);

		while (olvaso.hasNextLine()) {
			String sor = olvaso.nextLine();
			String[] darabolt = sor.split(",");
			if (darabolt.length == 4) {
				char betujel = darabolt[0].charAt(0);
				int prioritas = Integer.parseInt(darabolt[1]);
				int inditasi_ido = Integer.parseInt(darabolt[2]);
				int cpu_loketido = Integer.parseInt(darabolt[3]);
				Taszk uj = new Taszk(betujel, prioritas, inditasi_ido, cpu_loketido);
				taszkok.add(uj);
			}
		}

		olvaso.close();

		// System.out.println("Beolvasas kesz!");

		taszkok.sort(new Comparator<Taszk>() {
			@Override
			public int compare(Taszk o1, Taszk o2) {
				return o1.betujel - o2.betujel;
			}
		});

		taszkok.sort(new Comparator<Taszk>() {
			@Override
			public int compare(Taszk o1, Taszk o2) {
				return o1.inditasi_ido - o2.inditasi_ido;
			}
		});

		// System.out.println("Rendezes kesz!");

		Utemezo utemezo = new Tobbszintu();

		int ido = 0;

		while (ido <= taszkok.get(taszkok.size() - 1).inditasi_ido || utemezo.szeretnekDolgozni()) {
			for (Taszk taszk : taszkok) {
				if (taszk.inditasi_ido == ido) {
					utemezo.hozzaad(taszk);
				}
			}
			utemezo.dolgozz();
			++ido;
		}

		//System.out.println("Szimulacio kesz!");

		System.out.print("\n");
		boolean marVoltKiiras = false;
		for (Taszk taszk : taszkok) {
			if (marVoltKiiras) {
				System.out.print(",");
			}
			System.out.print(taszk.betujel + ":" + taszk.varakozasi_ido);
			marVoltKiiras = true;
		}
		System.out.print("\n");

	}

}
