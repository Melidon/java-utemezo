package utemezo;

public class Taszk {

	public char betujel;
	public int prioritas;
	public int inditasi_ido;
	public int cpu_loketido;
	public int varakozasi_ido;

	public Taszk(char betujel, int prioritas, int inditasi_ido, int cpu_loketido) {
		this.betujel = betujel;
		this.prioritas = prioritas;
		this.inditasi_ido = inditasi_ido;
		this.cpu_loketido = cpu_loketido;
		this.varakozasi_ido = 0;
	}

	public void print() {
		System.out.println(betujel + " " + prioritas + " " + inditasi_ido + " " + cpu_loketido + " " + varakozasi_ido);
	}

}
