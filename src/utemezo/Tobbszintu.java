package utemezo;

public class Tobbszintu implements Utemezo {

	private Utemezo[] utemezok;

	public Tobbszintu() {
		this.utemezok = new Utemezo[2];
		this.utemezok[0] = new SJF();
		this.utemezok[1] = new RR(2);
	}

	@Override
	public void hozzaad(Taszk taszk) {
		// System.out.println("Taszk hozzaadva a tobbszintuhez!");
		if (taszk.cpu_loketido > 0) {
			switch (taszk.prioritas) {
			case 1:
				this.utemezok[0].hozzaad(taszk);
				break;
			case 0:
				this.utemezok[1].hozzaad(taszk);
				break;
			}
		}
	}

	@Override
	public boolean szeretnekDolgozni() {
		for (Utemezo utemezo : this.utemezok) {
			if (utemezo.szeretnekDolgozni()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void dolgozz() {
		Utemezo dolgozo = null;
		for (Utemezo utemezo : this.utemezok) {
			if (utemezo.szeretnekDolgozni()) {
				dolgozo = utemezo;
				break;
			}
		}
		if (dolgozo != null) {
			// System.out.println("Tobbszintu dolgozik!");
			dolgozo.dolgozz();
		}
		for (Utemezo utemezo : this.utemezok) {
			if (utemezo != dolgozo) {
				utemezo.varakozz();
			}
		}
	}

	@Override
	public void varakozz() {
		for (Utemezo utemezo : this.utemezok) {
			utemezo.varakozz();
		}
	}

}
