package utemezo;

import java.util.ArrayList;
import java.util.Comparator;

public class SJF implements Utemezo {

	private ArrayList<Taszk> taszkok;

	private Taszk aktualis;

	public SJF() {
		this.taszkok = new ArrayList<Taszk>();
		this.aktualis = null;
	}

	@Override
	public void hozzaad(Taszk taszk) {
		// System.out.println(" Taszk hozzaadva az SJF-hez!");
		if (taszk.cpu_loketido > 0) {
			this.taszkok.add(taszk);
			this.taszkok.sort(new Comparator<Taszk>() {
				@Override
				public int compare(Taszk o1, Taszk o2) {
					return o1.cpu_loketido - o2.cpu_loketido;
				}
			});
		}
	}

	@Override
	public boolean szeretnekDolgozni() {
		return this.aktualis != null || this.taszkok.size() > 0;
	}

	@Override
	public void dolgozz() {
		if (!this.szeretnekDolgozni()) {
			return;
		}
		// System.out.println(" SJF dolgozik!");
		if (this.aktualis == null) {
			this.aktualis = this.taszkok.remove(0);
		}
		if (Main.elozo != this.aktualis.betujel) {
			System.out.print(this.aktualis.betujel);
			Main.elozo = this.aktualis.betujel;
		}
		--this.aktualis.cpu_loketido;
		for (Taszk taszk : this.taszkok) {
			++taszk.varakozasi_ido;
		}
		if (this.aktualis.cpu_loketido == 0) {
			this.aktualis = null;
		}
	}

	@Override
	public void varakozz() {
		if (this.aktualis != null) {
			this.hozzaad(aktualis);
			this.aktualis = null;
		}
		for (Taszk taszk : this.taszkok) {
			++taszk.varakozasi_ido;
		}
	}

}
