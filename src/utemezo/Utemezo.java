package utemezo;

public interface Utemezo {

	public void hozzaad(Taszk taszk);

	public boolean szeretnekDolgozni();

	public void dolgozz();

	public void varakozz();

}
