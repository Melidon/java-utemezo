package utemezo;

import java.util.ArrayList;

public class RR implements Utemezo {

	private ArrayList<Taszk> taszkok;

	private int maxFutasiIdo;

	private int mennyitFutottAzAktualis;

	private Taszk aktualis;

	public RR(int maxFutasiIdo) {
		this.taszkok = new ArrayList<Taszk>();
		this.maxFutasiIdo = maxFutasiIdo;
		this.mennyitFutottAzAktualis = 0;
		this.aktualis = null;
	}

	@Override
	public void hozzaad(Taszk taszk) {
		// System.out.println(" Taszk hozzaadva az RR-hez!");
		if (taszk.cpu_loketido > 0) {
			this.taszkok.add(taszk);
		}
	}

	@Override
	public boolean szeretnekDolgozni() {
		return this.aktualis != null || this.taszkok.size() > 0;
	}

	@Override
	public void dolgozz() {
		if (aktualis != null) {
			if (this.aktualis.cpu_loketido == 0) {
				this.aktualis = null;
				this.mennyitFutottAzAktualis = 0;
			}
			if (this.mennyitFutottAzAktualis >= this.maxFutasiIdo) {
				this.taszkok.add(this.aktualis);
				this.aktualis = null;
				this.mennyitFutottAzAktualis = 0;
			}
		}
		if (!this.szeretnekDolgozni()) {
			return;
		}
		// System.out.println(" RR dolgozik!");

		if (this.aktualis == null) {
			this.aktualis = this.taszkok.remove(0);
		}
		if (Main.elozo != this.aktualis.betujel) {
			System.out.print(this.aktualis.betujel);
			Main.elozo = this.aktualis.betujel;
		}
		--this.aktualis.cpu_loketido;
		++this.mennyitFutottAzAktualis;
		for (Taszk taszk : this.taszkok) {
			++taszk.varakozasi_ido;
		}
	}

	@Override
	public void varakozz() {
		if (this.aktualis != null) {
			this.hozzaad(aktualis);
			this.aktualis = null;
			this.mennyitFutottAzAktualis = 0;
		}
		for (Taszk taszk : this.taszkok) {
			++taszk.varakozasi_ido;
		}
	}

}
